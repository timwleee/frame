class AddAttachmentImageToCreators < ActiveRecord::Migration
  def self.up
    change_table :creators do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :creators, :image
  end
end
