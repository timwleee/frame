class AddLocationColumnToCreators < ActiveRecord::Migration
  def change
    add_column :creators, :location, :string
  end
end
