class BookingsController < ApplicationController
    def create
        bid = Bid.find(params[:id])
        event = bid.event
        booking = Booking.new(bid:Bid.find(params[:id]), user:User.find(current_user.id), creator:bid.creator, event:event)
        if booking.valid?
            booking.save
            flash[:messages] = ["Congratulations! You've booked this event!"]
        end
        redirect_to "/users/#{params[:id]}"
    end

    def show
        if current_user.type == "User"
            @bookings = Booking.where(user:current_user)
        else
            @bid = Bid.where(creator_id:current_user.id)
            @bookings = Booking.joins(:bid).where(bid_id:@bid)
        end
        render 'show'
    end
end
