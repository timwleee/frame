class AddBidColumnToEvents < ActiveRecord::Migration
  def change
    add_column :events, :bids, :integer
  end
end
