class AddBidColumnToBookings < ActiveRecord::Migration
  def change
    add_reference :bookings, :bid, index: true, foreign_key: true
  end
end
