class BidsController < ApplicationController
    def create
        bid = Bid.new(event:Event.find(params[:id]), creator:Creator.find(current_user.id))
        if bid.valid?
            bid.save
            flash[:messages] = ["Bid placed"]
        end
        redirect_to '/creators/browse'
    end

    def show
        @bids = Bid.where(event_id:params[:id])
        @event = Event.find(params[:id])
        @bookings = Booking.all
    end

    def destroy
        creator = Creator.find(current_user)
        event = Event.find(params[:id])
        Bid.find_by(event:event,creator:creator).destroy
        redirect_to '/creators/browse'
      end

end