class CreateBids < ActiveRecord::Migration
  def change
    create_table :bids do |t|
      t.references :event, index: true, foreign_key: true
      t.references :creator, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
