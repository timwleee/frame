class UsersController < ApplicationController 


    def index
        reset_session
    end
    
    def new
    end
    
    def create
        user = User.new(type:"User", first_name:params[:first_name], last_name:params[:last_name], location:params[:location],email:params[:email], password:params[:password], password_confirmation:params[:password_confirmation])
            if user.valid?
                user.save
                flash[:messages] = ["Registered successfully!"]
                puts flash[:messages]
                redirect_to "/"
            else
                flash[:messages] = user.errors.full_messages
                puts flash[:messages]
                redirect_to "/"
            end
        end
    
    def show
        @events = Event.where(user_id:current_user.id)
        @bids = Bid.find_by(event:@events)
        @reviews = Review.where(reviewed_for:params[:id], reviewed_user_type:"User")

        if current_user.type == "User"
            @user = User.find(current_user.id)
            @booking = Booking.where(bids:@bids)
            @bookings = Booking.where(user:current_user)
            @bid = Bid.joins(:event).where(event_id:@events)
        elsif current_user.type == "Creator"
            @user = User.find(params[:id])
            @reviews = Review.where(reviewed_for:@user.id, reviewed_user_type:"User")
        end
    end

    def browse
        @creators = Creator.all
        @reviews = Review.where(reviewed_user_type:"Creator")
    
    end

    def edit
        render 'edit'
    end

    def update
        user = User.find(params[:id])
        user.update(first_name:params[:first_name], last_name:params[:last_name], email:params[:email], password:params[:password], password_confirmation:params[:password_confirmation])
        redirect_to "/users/#{params[:id]}"
    end

    def destroy
        User.find(params[:id]).destroy
        redirect_to "/"
    end
end
