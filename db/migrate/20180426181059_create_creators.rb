class CreateCreators < ActiveRecord::Migration
  def change
    create_table :creators do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :portfolio
      t.string :type
      t.string :password_digest
      t.integer :rating

      t.timestamps null: false
    end
  end
end
