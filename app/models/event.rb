class Event < ActiveRecord::Base
  belongs_to :user
  belongs_to :booking
  has_many :bids, dependent: :destroy
  has_many :creators, through: :bids
end
