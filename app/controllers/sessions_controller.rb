class SessionsController < ApplicationController
    def new
    end
  
    def create
      u = User.find_by(email:params[:email])
      c = Creator.find_by(email:params[:email])
        if u && u.authenticate(params[:password])
          session[:user_id] = u.id
          session[:type] = "user"
          flash[:messages] = ['Signed in successfully!']
          redirect_to "/users/#{current_user.id}"
        elsif c && c.authenticate(params[:password])
          session[:creator_id] = c.id
          session[:type] = "creator"
          flash[:messages] = ['Signed in successfully!']
          redirect_to "/creators/#{current_user.id}"
        else
          flash[:messages] = ["Invalid Login or Password"]
          puts flash[:messages]
          redirect_to '/'
        end
    end
  
    def destroy
      session[current_user] = nil
      flash[:messages] = ["You've logged out"]         
      redirect_to '/' 
    end
  end
  