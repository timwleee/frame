class ChangeEndtimeToBeDatetimeInEvents < ActiveRecord::Migration
  def change
    change_column :events, :endtime, :datetime
  end
end
