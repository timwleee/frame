class RemoveRatingColumnFromCreators < ActiveRecord::Migration
  def change
    remove_column :creators, :rating, :integer
  end
end
