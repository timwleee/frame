class CreatorsController < ApplicationController
    def new
    end
    
    def create 
        c = Creator.new(type:"Creator", first_name:params[:first_name], last_name:params[:last_name], email:params[:email], image:params[:image], location:params[:location],portfolio:params[:portfolio], info:params[:info], password:params[:password], password_confirmation:params[:password_confirmation])
        if c.valid?
            c.save
            flash[:messages] = ['Success!']
            redirect_to '/'
        else
            flash[:messages] = u.errors.full_messages
            redirect_to '/creators/new'
        end
    end
    
    def find_jobs
        @creator = Creator.find_by(params[:user_id])
        @events = Event.all
    end
    
    def browse
        @events = Event.all
    end
    
    def show
        @bids = Bid.joins(:event).where(creator:params[:id])
        @reviews = Review.where(reviewed_for:params[:id], reviewed_user_type:"Creator")
        
        if current_user.type == "Creator"
            @creator = Creator.find(current_user.id)
            @bid = Bid.where(creator_id:current_user)
            @bookings = Booking.all
        elsif current_user.type == "User"
            @creator = Creator.find(params[:id])
            @reviews = Review.where(reviewed_for:@creator.id, reviewed_user_type:"Creator")
            @booking = Booking.joins(:bid).where(user_id:current_user.id, bid:@bids)
        end

    end

    def update
        @creator = Creator.find(current_user.id)
        @creator.update(creator_params)
        @creator.save
        redirect_to "/creators/#{current_user.id}"
    end

    private

        def creator_params
            params.permit(:image)
        end
    
    end
    