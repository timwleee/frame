class Booking < ActiveRecord::Base
  belongs_to :user
  belongs_to :creator
  belongs_to :event
  belongs_to :bid
end

