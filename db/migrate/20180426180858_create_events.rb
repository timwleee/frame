class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :event
      t.string :price
      t.string :location
      t.date :date
      t.string :service
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
