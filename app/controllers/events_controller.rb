class EventsController < ApplicationController
    def create
        event = Event.create(service:params[:service], notes:params[:notes], starttime:params[:starttime], endtime:params[:endtime], event:params[:event], price:params[:price], location:params[:location], date:params[:date], user_id:current_user.id)
            if event.valid?
                event.save
                flash[:messages] = ["Thanks for creating your event!"]
            end
        redirect_to "/users/#{current_user.id}"
    end

    def show
        @bids = Bid.where(event:params[:id])
    end

    def destroy
        event = Event.find_by(id:params[:id])
        event.destroy
        flash[:messages] = ["Event deleted"]
        redirect_to "/users/#{current_user.id}"
    end

end
