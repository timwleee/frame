class AddWrittenByColumnToReviews < ActiveRecord::Migration
  def change
    add_column :reviews, :written_by, :string
  end
end
