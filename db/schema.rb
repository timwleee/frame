# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180716235234) do

  create_table "bids", force: :cascade do |t|
    t.integer  "event_id"
    t.integer  "creator_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "bids", ["creator_id"], name: "index_bids_on_creator_id"
  add_index "bids", ["event_id"], name: "index_bids_on_event_id"

  create_table "bookings", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "creator_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "event_id"
    t.integer  "bid_id"
  end

  add_index "bookings", ["bid_id"], name: "index_bookings_on_bid_id"
  add_index "bookings", ["creator_id"], name: "index_bookings_on_creator_id"
  add_index "bookings", ["event_id"], name: "index_bookings_on_event_id"
  add_index "bookings", ["user_id"], name: "index_bookings_on_user_id"

  create_table "creators", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "portfolio"
    t.string   "type"
    t.string   "password_digest"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.text     "info"
    t.integer  "rating"
    t.string   "location"
  end

  create_table "events", force: :cascade do |t|
    t.string   "event"
    t.string   "price"
    t.string   "location"
    t.date     "date"
    t.string   "service"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "bids"
    t.string   "duration"
    t.text     "notes"
    t.datetime "starttime"
    t.datetime "endtime"
    t.integer  "booking_id"
  end

  add_index "events", ["booking_id"], name: "index_events_on_booking_id"
  add_index "events", ["user_id"], name: "index_events_on_user_id"

  create_table "reviews", force: :cascade do |t|
    t.text     "content"
    t.integer  "user_id"
    t.integer  "creator_id"
    t.string   "reviewed_for"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "written_by"
    t.integer  "rating"
    t.string   "reviewed_user_type"
  end

  add_index "reviews", ["creator_id"], name: "index_reviews_on_creator_id"
  add_index "reviews", ["user_id"], name: "index_reviews_on_user_id"

  create_table "users", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "password_digest"
    t.string   "type"
    t.integer  "rating"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "location"
  end

end
