class AddBookingColumnToEvents < ActiveRecord::Migration
  def change
    add_reference :events, :booking, index: true, foreign_key: true
  end
end
