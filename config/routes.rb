Rails.application.routes.draw do
  get '/' => 'users#index'

  get 'users/new' => 'users#new'
  post 'users/create'=> 'users#create'
  get 'users/browse' => 'users#browse'
  get 'users/:id' => 'users#show'
  get 'users/:id/edit' => 'users#edit'
  patch 'users/:id' => 'users#update'

  get 'creators/new' => 'creators#new'
  get 'creators/browse' => 'creators#browse'
  post 'creators/create' => 'creators#create'
  get 'creators/find_jobs' => 'creators#find_jobs'
  get 'creators/:id' => 'creators#show'
  put 'creators/:id' => 'creators#update'

  get 'events/new' => 'events#new'
  post 'events/create' => 'events#create'
  get 'events/:id' => 'events#show'
  delete 'events/:id' => 'events#destroy'

  post 'sessions/create' => 'sessions#create'
  delete 'sessions/destroy/:id' => 'sessions#destroy'

  post 'bids/create/:id' => 'bids#create'
  get 'bids/:id' => 'bids#show'
  delete 'bids/:id' => 'bids#destroy'

  get 'reviews/:id/new' => 'reviews#new'
  post 'reviews/:id/create' => 'reviews#create'
  get 'reviews/:id' => 'reviews#show'

  post 'bookings/:id' => 'bookings#create'
  get 'bookings/:id' => 'bookings#show'
end
