class User < ActiveRecord::Base
  has_secure_password
  validates :first_name, :last_name, :password, :email, presence: true
  validates :email, uniqueness: true
  has_many :bookings, through: :events, dependent: :destroy
  has_many :events, dependent: :destroy
  has_many :reviews, dependent: :destroy
end
