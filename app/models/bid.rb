class Bid < ActiveRecord::Base
  belongs_to :event
  belongs_to :creator
  belongs_to :booking
end
