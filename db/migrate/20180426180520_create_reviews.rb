class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.text :content
      t.references :user, index: true, foreign_key: true
      t.references :creator, index: true, foreign_key: true
      t.string :reviewed_for

      t.timestamps null: false
    end
  end
end
