class AddInfoColumnToCreators < ActiveRecord::Migration
  def change
    add_column :creators, :info, :text
  end
end
