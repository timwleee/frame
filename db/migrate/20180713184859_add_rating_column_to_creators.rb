class AddRatingColumnToCreators < ActiveRecord::Migration
  def change
    add_column :creators, :rating, :integer
  end
end
