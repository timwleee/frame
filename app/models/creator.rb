class Creator < ActiveRecord::Base
  has_secure_password
  validates :first_name, :last_name, :password, :email, presence: true
  has_many :bids
  has_many :reviews
  has_many :events, through: :bids
  has_many :bookings, through: :bids
  has_attached_file :image, styles: { large: "600x600>", medium: "300x300>", thumb: "150x150#>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
end
