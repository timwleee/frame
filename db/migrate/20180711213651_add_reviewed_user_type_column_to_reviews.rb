class AddReviewedUserTypeColumnToReviews < ActiveRecord::Migration
  def change
    add_column :reviews, :reviewed_user_type, :string
  end
end
