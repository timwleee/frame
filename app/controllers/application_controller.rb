class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def current_user
    if session[:user_id]
      User.find(session[:user_id])
    elsif session[:creator_id]
      Creator.find(session[:creator_id])  
    end
  end

  
  def require_login
    if session[:user_id] == nil
      redirect_to '/'
    elsif session[:creator_id] == nil
      redirect_to '/'
    end
  end
  
  def require_correct_user
    user = User.find(params[:id])
    redirect_to current_user if current_user != user
  end


  helper_method :current_user, :require_login, :require_correct_user
end
