class ReviewsController < ApplicationController
    def create
        if current_user.type == "Creator"
            review = Review.new(content:params[:content], user_id:params[:id],creator_id:current_user.id, rating:params[:rating], reviewed_for:params[:id], written_by:current_user.first_name + " " + current_user.last_name, reviewed_user_type: "User")
            if review.valid?
                review.save
                flash[:messages] = ["Thanks for leaving a review!"]
            end
            redirect_to "/creators/#{current_user.id}"

        elsif current_user.type == "User"
            review = Review.new(content:params[:content], user_id:current_user.id, creator_id:params[:id], rating:params[:rating], reviewed_for:params[:id], written_by:current_user.first_name + " " + current_user.last_name, reviewed_user_type: "Creator")
            if review.valid?
                review.save
                flash[:messages] = ["Thanks for leaving a review!"]
            end
            redirect_to "/users/#{current_user.id}"
        end
    end

    def new
        if current_user.type == "Creator"
            @user = User.find(params[:id])
        else
            @creator = Creator.find(params[:id])
        end
    end

    def show
        if current_user.type == "Creator"
            @creator = Creator.find(params[:id])
            @reviews = Review.where(reviewed_for:@creator.id, reviewed_user_type: "Creator")

        elsif current_user.type == "User"
            @user = User.find(params[:id])
            @reviews = Review.where(reviewed_for:@user.id, reviewed_user_type: "User")

        end
        
    end
end